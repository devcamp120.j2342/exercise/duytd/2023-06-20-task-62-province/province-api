package com.devcamp.provinceapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.provinceapi.models.Province;

public interface ProvinceRepository extends JpaRepository<Province, Long>{
    Province findDistrictByCode(String code);
}
