package com.devcamp.provinceapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.provinceapi.models.District;

public interface DistrictRepository extends JpaRepository<District, Long>{
    District findById(int id);
}
