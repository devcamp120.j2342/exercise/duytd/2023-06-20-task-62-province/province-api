package com.devcamp.provinceapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.provinceapi.models.Ward;

public interface WardRepository extends JpaRepository<Ward, Long>{
    
}
