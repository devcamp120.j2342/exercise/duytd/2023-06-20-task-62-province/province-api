package com.devcamp.provinceapi.models;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "ward")
public class Ward {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String prefix;

    @ManyToOne
    @JoinColumn(name = "district_id")
    @JsonIgnore
    private District cDistrict;

    public Ward() {
    }

    public Ward(int id, String name, String prefix, District cDistrict) {
        this.id = id;
        this.name = name;
        this.prefix = prefix;
        this.cDistrict = cDistrict;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public District getDistrict() {
        return cDistrict;
    }

    public void setDistrict(District cDistrict) {
        this.cDistrict = cDistrict;
    }

}
