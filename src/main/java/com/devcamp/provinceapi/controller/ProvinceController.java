package com.devcamp.provinceapi.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.provinceapi.models.District;
import com.devcamp.provinceapi.models.Province;
import com.devcamp.provinceapi.models.Ward;
import com.devcamp.provinceapi.repository.DistrictRepository;
import com.devcamp.provinceapi.repository.ProvinceRepository;
import com.devcamp.provinceapi.repository.WardRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class ProvinceController {
    @Autowired
    private ProvinceRepository provinceRepository;

    @Autowired
    private DistrictRepository districtRepository;

    @CrossOrigin
    @GetMapping("/province5")
    public ResponseEntity<Page<Province>> getFileProvince(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "5") int size) {
        try {
            PageRequest pageRequest = PageRequest.of(page, size);
            Page<Province> provincePage = provinceRepository.findAll(pageRequest);
            return new ResponseEntity<>(provincePage, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/districts")
    public ResponseEntity<List<District>> getAllDistricts(
            @RequestParam(value = "code") String code) {

        try {
            Province province = provinceRepository.findDistrictByCode(code);
            if (province != null) {
                List<District> districts = new ArrayList<>();
                districts.addAll(province.getDistrict());
                return new ResponseEntity<>(districts, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/wards")
    public ResponseEntity<List<Ward>> getAllWards(
        @RequestParam(value="id") int id) {
        try {
          District district = districtRepository.findById(id);
          if(district != null) {
            List<Ward> ward = new ArrayList<>();
            ward.addAll(district.getWards());
            return new ResponseEntity<>(ward, HttpStatus.OK);
          }else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
          }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
